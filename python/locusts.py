from locust import Locust, TaskSet, task
import random
import requests
MAX_VALUE = 10042
hostname = 'http://127.0.0.1:8000'
stats_name = 'locust_test'
account_name = 'locust_acc'

def update_stats(obj):
    value = random.triangular(0.01, MAX_VALUE)
    requests.post(hostname + '/stats?account_name=%s&stats_name=%s&value=%s'\
                                        % (account_name, stats_name, value))
def create_stat(obj):
    stats_name = [random.choice('abcdefghijklmnopqrstuvxyz') for i in range(10)]
    requests.post(hostname + '/stats/init?account_name=%s&stats_name=%s'\
                            % (account_name,stats_name))

class UpdateStatsSet(TaskSet):
    #hostname = 'https://online-stats.herokuapp.com'
    tasks = {create_account: 1, update_stats : 50}

class MyLocust(Locust):
    task_set = UpdateStatsSet
    min_wait = 5000
    max_wait = 15000

#if __name__ == '__main__':
#    u = UpdateStatsSet()
#    for i in range(5000):
#        u.update_stats()

