TODO:
=======================
0. Migrate to using serverless architecture zappos or chalice
1. Simplify the data store to use less space
2. Find a way to whittle-down the floating-point computation time.
3. Add support for hyperloglog algorithm, so that unique values can be tracked.
4. Add a way to keep periodic snapshot of the tracked moment statistics
4. Add support for maximum and minimum values seen.
5. Add support for parameter modeling and estimation like (this
   https://github.com/anandjeyahar/statistical-analysis-python-tutorial).
    a, If you can also implement kernel estimation great, but online kernel estimation methods seem
    rare(http://stats.stackexchange.com/questions/133080/density-estimation-for-streams-of-data/236351#236351)

